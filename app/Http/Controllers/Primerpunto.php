<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Primerpunto extends Controller
{
    public function index()
    {
        return view('1.index');
    }

    public function solucion(Request $request)
    {

        $numeros1 = $request->numeros;

        $integers   = array();
        $result     = 0;

        for($i = 0; $i < count($numeros1); $i++){
            $integers[] = intval($numeros1[$i]);
        }

        for($i = 0; $i < count($integers); $i++)
        {
            if($integers[$i] == 8)
            {
                $result = $result + 5;
            }else{
                if($integers[$i] % 2 == 0){
                    $result = $result + 1;
                }else{
                    $result = $result + 3;
                }
            }
        }
        return redirect()->route('punto_uno')->with('mensaje','Resultado: '.$result);
    }
}
