<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Segundopunto extends Controller
{
    public function index(){
        return view('2.index');
    }

    public function solucion(Request $request)
    {
        $num1   = $request->valor_inicial;
        $num2   = $request->valor_final;
        $result = 100;
        $datos  =[10, 20, 30, 40, 50, 60, 70, 80, 90, 100];

        if (in_array($num1, $datos) AND in_array($num2, $datos)){
            if($num1 > $num2){
                $result = 0;
            }
            else if($num1<0 || $num2<0)
            {
                    $result = -1;
            }
            
            else
            {
                $posicion1      = array_search($num1, $datos);
                $posicion2      = array_search($num2, $datos);
    
                for($i = $posicion1; $i <= $posicion2; $i++)
                {
                    $rango_valores[] = $datos[$i];
                }
                $result         = array_sum($rango_valores);
            }
           
        }else if (in_array($num1, $datos) AND !in_array($num2, $datos))
        {
            if ($num1 <= $datos[9] AND $num2 > $datos[9]) {
                $posicion1      = array_search($num1, $datos);
    
                for($i = $posicion1; $i <= 9; $i++)
                {
                    $rango_valores[] = $datos[$i];
                }
                $result         = array_sum($rango_valores);
            }
        }
        else{
            if($num1<0 || $num2<0)
            {
                $result = -1;
            }else if($num1 > $datos[9] AND $num2 > $datos[9])
            {
                $result = 0;
            }
            else
            {
                return redirect()->route('punto_dos')->with('error','El valor inicial no se encuentra en los datos: recuerde usar un numero de 10 en 10, hasta 100.');
            }
        }
        return redirect()->route('punto_dos')->with('mensaje','Resultado: '.$result);
    }
}
