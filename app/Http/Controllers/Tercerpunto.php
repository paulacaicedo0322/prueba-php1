<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Tercerpunto extends Controller
{
    public function index(){
        return view('3.index');
    }

    public function solucion(Request $request)
    {
        $time = $request->time;
        $result = 0;
        $error = "";

        if (strpos($time, ":") != false) 
        {
            $hora = explode(':', $request->time);
            
            if (count($hora) == 2) 
            {
                if (is_numeric($hora[0]) AND is_numeric($hora[1])) {
                    $integers_time   = array();
    
                    for($i = 0; $i < count($hora); $i++){
                        $integers_time[] = intval($hora[$i]);
                    }

                    if ($integers_time[0] <= 12 AND $integers_time[1] <= 59) {
                        $angulo_hora        = $integers_time[0] * 30;
                        $angulo_minutero    = $integers_time[1] * 6;
                
                        if ($angulo_hora < $angulo_minutero) {
                            $result = $angulo_hora;
                        }else{
                            $result = $angulo_minutero;
                        }
                        return redirect()->route('punto_tres')->with('mensaje','Resultado: '.$result.'°');
                    }
                    else{
                        $error  = "El formato es valido, pero la cantidad de horas o minutos no.";
                    }
                }
                else{
                    $error = "No envio datos númericos";
                }
            }
            
        }else{
            $error = "Formato de hora no valido. Ejemplo: '05:12' ";
        }
       
        return redirect()->route('punto_tres')->with('error' , $error);
    }
}
