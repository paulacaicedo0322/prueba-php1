<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Quintopunto extends Controller
{
    public function index(){
        return view('5.index');
    }

    public function solucion(Request $request){
        $l = $request->lado;
        $n = $request->modulo;
        $respuesta = 0;

        if (1 <= $l AND $l <= 1000000 AND 0 <= $n AND $n <= $l*$l)
        {
            if ($l > 0 AND $n == 0) 
            {
                $respuesta = 0;
            }else{
                $respuesta = $l*4;
            }
            
        }else if ($l == 0 AND $n == 0) 
        {
            return redirect()->route('punto_cinco')->with('error','ERROR.');
        }else{
            return redirect()->route('punto_cinco')->with('error','Los datos ingresados no cumplen los requisitos.');
        }

        return redirect()->route('punto_cinco')->with('mensaje','Resultado: '.$respuesta);
    }
}
