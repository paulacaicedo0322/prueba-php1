<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Cuartopunto extends Controller
{
    public function index()
    {
        $matriz[0] = [0,   2,  5,  9, 14, 20];
        $matriz[1] = [1,   4,  8, 13, 19, 26];
        $matriz[2] = [3,   7, 12, 18, 25, 33];
        $matriz[3] = [6,  11, 17, 24, 32, 41];
        $matriz[4] = [10, 16, 23, 31, 40, 50];
        $matriz[5] = [15, 22, 30, 39, 49, 60];
        $matriz[6] = [21, 29, 38, 48, 59, 71];
        $matriz[7] = [28, 37, 47, 58, 70, 83];
        $matriz[8] = [36, 46, 57, 69, 82, 96];
        $matriz[9] = [45, 56, 68, 81, 95, 110];

        $filas[0] = [0];
        $filas[1] = [1];
        $filas[2] = [2];
        $filas[3] = [3];
        $filas[4] = [4];
        $filas[5] = [5];
        $filas[6] = [6];
        $filas[7] = [7];
        $filas[8] = [8];
        $filas[9] = [9];

        return view('4.index', compact('matriz', 'filas'));
        
    }

    public function solucion(Request $request)
    {
        $p1 = $request->posicion1;
        $p2 = $request->posicion2;

        $matriz[0] = [0,   2,  5,  9, 14, 20];
        $matriz[1] = [1,   4,  8, 13, 19, 26];
        $matriz[2] = [3,   7, 12, 18, 25, 33];
        $matriz[3] = [6,  11, 17, 24, 32, 41];
        $matriz[4] = [10, 16, 23, 31, 40, 50];
        $matriz[5] = [15, 22, 30, 39, 49, 60];
        $matriz[6] = [21, 29, 38, 48, 59, 71];
        $matriz[7] = [28, 37, 47, 58, 70, 83];
        $matriz[8] = [36, 46, 57, 69, 82, 96];
        $matriz[9] = [45, 56, 68, 81, 95, 110];

        $filas[0] = [0];
        $filas[1] = [1];
        $filas[2] = [2];
        $filas[3] = [3];
        $filas[4] = [4];
        $filas[5] = [5];
        $filas[6] = [6];
        $filas[7] = [7];
        $filas[8] = [8];
        $filas[9] = [9];

        if ($p1 < 0 || $p2 < 0) {
            return redirect()->route('punto_cuatro')->with('error','No hay filas ni columnas negativas.');
        }
        if ($p1 > 9) {
            return redirect()->route('punto_cuatro')->with('error','Excediste el número de filas.');
        }else if($p2 > 5)
        {
            return redirect()->route('punto_cuatro')->with('error','Excediste el número de columnas.');
        }else{
            $result = $matriz[$p1][$p2];

            return redirect()->route('punto_cuatro')->with('mensaje','Resultado: '.$result);
        }
    }
}
