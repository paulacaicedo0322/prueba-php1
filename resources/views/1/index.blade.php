@extends('layout.base')

@section('css_extra')	

@endsection
@section('content')

<h2>Punto uno</h2>
<h4>Agregue los número que desee</h4>
<div class="card" style="width: 18rem;">
    <div class="card-body">
        <form id="validar" action="{{route('punto1_solucion')}}" method="post" >
            @csrf   
            <table id="numeros_array" class="table table-bordered border-primar" style="width: 50px">
                <thead class="table-dark">
                    <tr>
                        <th style="text-align: center">Números</th> 
                    </tr>
                </thead>
                <tbody>
                    <tr> 
                        <td id="col0"> 
                            <input type="number" name="numeros[]" required>
                        </td> 
                    </tr>
                </tbody>  
            </table> 
            <table>
                <br>
                <tr> 
                    <td><button type="button" class="btn btn-sm btn-info" onclick="agregarFila()">Agregar</button></td>
                    <td><button type="button" class="btn btn-sm btn-danger" onclick="eliminarFila()">Eliminar</button></td>
                    <td><button type="submit" class="btn btn-sm btn-success">Probar</button></td> 
                </tr>  
            </table>
        </form>
    </div>
    <div>
            @if(Session::has('mensaje'))
      <div class="alert alert-info" role="alert">
        {{ Session::get('mensaje') }}
      </div>
    @endif
    </div>
  </div>


@endsection
@section('js_extra')
<script>
    //funcion que agregar una fila para agregar mas alquileres
    function agregarFila()
    { 
        var table = document.getElementById('numeros_array');
        var rowCount = table.rows.length;
        var cellCount = table.rows[0].cells.length; 
        var row = table.insertRow(rowCount);
        for(var i =0; i <= cellCount; i++)
        {
            var cell = 'cell'+i;
            cell = row.insertCell(i);
            var copycel = document.getElementById('col'+i).innerHTML;
            cell.innerHTML=copycel;
            if(i == 3)
            { 
                var radioinput = document.getElementById('col2').getElementsByTagName('input'); 
                for(var j = 0; j <= radioinput.length; j++)
                { 
                    if(radioinput[j].type == 'radio')
                    { 
                        var rownum = rowCount;
                        radioinput[j].name = 'gender['+rownum+']';
                    }
                }
            }
        }
    }

    //funcion que elimina una fila de alquileres
    function eliminarFila()
    {
        var table = document.getElementById('numeros_array');
        var rowCount = table.rows.length;
        if(rowCount > '2')
        {
            var row = table.deleteRow(rowCount-1);
            rowCount--;
        }else{
            Swal.fire({
                position: 'center',
                icon: 'error',
                title: 'Debe ingresar por lo menos un número',
                showConfirmButton: false,
                timer: 3000
            });
        }
    }
</script>
<!-- Alertas-->
<script>
    function blink_text()
    {
        $('#mensaje_error').fadeOut(700);
        $('#mensaje_error').fadeIn(700);
    }
    setInterval(blink_text,1000);
</script>
<!-- Validacion de formulario -->
<script>
    $('#validar').validate({
        reles: {
            'fecha_inicio[]': {
                required: true,
            },
            'fecha_fin[]': {
                required:true,
            },
            'id_cliente[]': {
                required:true,
            },
            'pelicula[]': {
                required:true,
            },
        },
        messages: {
            'fecha_inicio[]' : "Por favor ingrese la fecha inicial*",
            'fecha_fin[]'     : "Por favor ingrese la fecha final*",
            'id_cliente[]'    : "Por favor seleccione el cliente*",
            'pelicula[]'   : "Por favor seleccione la pelicula*",

        },
    });
</script>