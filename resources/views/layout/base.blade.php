<!doctype html>
	<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">
		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href=" {{asset('assets/bootstrap/css/bootstrap.min.css')}} ">
		<link href='https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>

		<script src="{{asset('assets/js/dash/modernizr.js')}}"></script> <!-- Modernizr -->
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<script src="https://code.iconify.design/2/2.2.1/iconify.min.js"></script>
		<script src="https://kit.fontawesome.com/09fda739b2.js" crossorigin="anonymous"></script>	
		  
		<title>Prueba desarrollador</title>
	</head>
	<body>
		<header>
			<nav class="navbar navbar-expand-lg navbar-light "  style="background-color: rgba(73, 41, 5); color:bisque;" >
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
				  <span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse"id="navbarText">
				  <ul class="navbar-nav mr-auto">
					<li class="nav-item active" style="color:bisque;">
					  <a class="nav-link"  href="#"><span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item" style="color:bisque;">
					  <a class="nav-link" href="{{route("punto_uno")}}" style="color:bisque;">Punto 1</a>
					</li>
					<li class="nav-item">
					  <a class="nav-link" href="{{route("punto_dos")}}" style="color:bisque;">Punto 2</a>
					</li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route("punto_tres")}}" style="color:bisque;">Punto 3</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route("punto_cuatro")}}" style="color:bisque;">Punto 4</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route("punto_cinco")}}" style="color:bisque;">Punto 5</a>
                    </li>
				  </ul>
				  <span class="navbar-text" style="color:bisque;">
					Prueba
				  </span>
				</div>
			  </nav>
		</header>
	<div style="margin: 2%;">
@yield('css_extra')

		@yield('content')
	</div>
	<main class="cd-main-content">
		<!-- main content here -->
	</main>
	
	{{-- script of login  --}}
	<script src="{{asset('assets/js/dash/jquery-2.1.4.js')}}"></script>
	<script src="{{asset('assets/js/dash/main.js')}}"></script>

	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	
	<script src="{{asset('assets/js/jquery.min.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
	<script src=" {{asset('assets/bootstrap/js/bootstrap.min.js')}} "></script>
	<script src=" {{asset('assets/bootstrap/js/bootstrap.bundle.min.js')}} "></script>
	<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@yield('js_extra')
</body>
</html>    
