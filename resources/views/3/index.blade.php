@extends('layout.base')

@section('css_extra')	

@endsection
@section('content')
<h2>Punto tres</h2>

<h4>Escriba una hora: </h4>
<div class="card" >
    <div class="card-body">
        <form  action="{{route('punto3_solucion')}}" method="post" >
            @csrf   
            <div class="row">
              <div class="col">
                <label for="">Hora</label>
                <input type="text" class="form-control" name="time" placeholder="12:30" required>
              </div>
              
            </div>
            <br> 
            <div>
                <button class="btn btn-success" type="submit">Probar</button>
            </div>
          </form>
    </div>
    <div>
        @if(Session::has('mensaje'))
            <div class="alert alert-info" role="alert">
            {{ Session::get('mensaje') }}
            </div>
        @endif
        @if(Session::has('error'))
        <div class="alert alert-danger" role="alert">
        {{ Session::get('error') }}
        </div>
    @endif
    </div>
  </div>


@endsection
@section('js_extra')
