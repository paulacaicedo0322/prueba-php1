@extends('layout.base')

@section('css_extra')	

@endsection
@section('content')

<div class="row" >
    <div class="col-4">
        <h2>Punto cinco</h2>

<h4>Escriba una hora: </h4>
        <form  action="{{route('punto5_solucion')}}" method="post" >
            @csrf   
            <div class="row">
              <div class="col">
                <label for="">Valor del lado</label>
                <input type="number" class="form-control" name="lado" required>
              </div>
              <div class="col">
                <label for="">Módulos</label>
                <input type="number" class="form-control" name="modulo" required>
              </div>
            </div>
            <br> 
            <div>
                <button class="btn btn-success" type="submit">Probar</button>
            </div>
          </form>
          <div>
            @if(Session::has('mensaje'))
                <div class="alert alert-info" role="alert">
                {{ Session::get('mensaje') }}
                </div>
            @endif
            @if(Session::has('error'))
            <div class="alert alert-danger" role="alert">
            {{ Session::get('error') }}
            </div>
        @endif
        </div>
    </div>
  </div>



@endsection
@section('js_extra')
