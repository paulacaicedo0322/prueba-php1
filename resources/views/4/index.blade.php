@extends('layout.base')

@section('css_extra')	

@endsection
@section('content')

<div class="row" >
    <div class="col-4">
        <h2>Punto cuatro</h2>

        <form  action="{{route('punto4_solucion')}}" method="post" >
            @csrf   
            <div class="row">
              <div class="col">
                <label for="">Número de fila</label>
                <input type="number" class="form-control" name="posicion1" required>
              </div>
              <div class="col">
                <label for="">Número de columna</label>
                <input type="number" class="form-control" name="posicion2" required>
              </div>
            </div>
            <br> 
            <div>
                <button class="btn btn-success" type="submit">Probar</button>
            </div>
          </form>
          <div>
            @if(Session::has('mensaje'))
                <div class="alert alert-info" role="alert">
                {{ Session::get('mensaje') }}
                </div>
            @endif
            @if(Session::has('error'))
            <div class="alert alert-danger" role="alert">
            {{ Session::get('error') }}
            </div>
        @endif
        </div>
    </div>
    <div class="col-8">
        <table id="numeros_array" class="table table-bordered border-primar" >
            <thead class="table-dark">
                <tr>
                    <th style="text-align: center"> - </th> 
                    <th style="text-align: center">0</th> 
                    <th style="text-align: center">1</th> 
                    <th style="text-align: center">2</th> 
                    <th style="text-align: center">3</th> 
                    <th style="text-align: center">4</th> 
                    <th style="text-align: center">5</th> 
                </tr>
            </thead>
            <tbody>
                <td style="text-align: center"  class="table-dark" rowspan="11">
                    <br>
                    @php 
                        for($i = 0; $i < 10; $i++ ) 
                        { 
                            echo $i.'</br></br><hr>'; 
                        }
                    @endphp
                </td>
              @foreach ($matriz as $f) 
                <tr>
                    <td style="text-align: center">{{$f[0]}}</td>
                    <td style="text-align: center">{{$f[1]}}</td>
                    <td style="text-align: center">{{$f[2]}}</td>
                    <td style="text-align: center">{{$f[3]}}</td>
                    <td style="text-align: center">{{$f[4]}}</td>
                    <td style="text-align: center">{{$f[5]}}</td>
                </tr>
              @endforeach
            </tbody>  
        </table> 
    </div>
  </div>



@endsection
@section('js_extra')
