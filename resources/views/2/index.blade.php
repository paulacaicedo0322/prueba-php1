@extends('layout.base')

@section('css_extra')	

@endsection
@section('content')
<h2>Punto dos</h2>

<h4>Agregue los números que desee</h4>
<div class="card" >
    <div class="card-body">
        <form  action="{{route('punto2_solucion')}}" method="post" >
            @csrf   
            <div class="row">
              <div class="col">
                <label for="">Valor inicial</label>
                <input type="number" class="form-control" name="valor_inicial" required>
              </div>
              <div class="col">
                <label for="">Valor final</label>
                <input type="number" class="form-control" name="valor_final" required>
              </div>
            </div>
            <br> 
            <div>
                <button class="btn btn-success" type="submit">Probar</button>
            </div>
          </form>
    </div>
    <div>
        @if(Session::has('mensaje'))
            <div class="alert alert-info" role="alert">
            {{ Session::get('mensaje') }}
            </div>
        @endif
        @if(Session::has('error'))
        <div class="alert alert-danger" role="alert">
        {{ Session::get('error') }}
        </div>
        @endif

    </div>
  </div>


@endsection
@section('js_extra')
