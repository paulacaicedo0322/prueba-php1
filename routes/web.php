<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Primerpunto;
use App\Http\Controllers\Segundopunto;
use App\Http\Controllers\Tercerpunto;
use App\Http\Controllers\Cuartopunto;
use App\Http\Controllers\Quintopunto;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

// punto uno 
Route::get('/punto-uno', [Primerpunto::class, 'index'])->name('punto_uno');
Route::post('/resultado-uno', [Primerpunto::class, 'solucion'])->name('punto1_solucion');

// punto dos 
Route::get('/punto-dos', [Segundopunto::class, 'index'])->name('punto_dos');
Route::post('/resultado-dos', [Segundopunto::class, 'solucion'])->name('punto2_solucion');


// punto tres 
Route::get('/punto-tres', [Tercerpunto::class, 'index'])->name('punto_tres');
Route::post('/resultado-tres', [Tercerpunto::class, 'solucion'])->name('punto3_solucion');

// punto cuatro 
Route::get('/punto-cuatro', [Cuartopunto::class, 'index'])->name('punto_cuatro');
Route::post('/resultado-cuatro', [Cuartopunto::class, 'solucion'])->name('punto4_solucion');

// punto cinco 
Route::get('/punto-cinco', [Quintopunto::class, 'index'])->name('punto_cinco');
Route::post('/resultado-cinco', [Quintopunto::class, 'solucion'])->name('punto5_solucion');